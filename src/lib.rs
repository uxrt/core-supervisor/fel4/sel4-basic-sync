// Copyright 2023 Andrew Warkentin
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.


//#![doc(html_root_url = "https://doc.robigalia.org/")]
#![no_std]
#![feature(naked_functions)]
#![feature(thread_local)]


///A library for parking and unparking threads.

extern crate sel4;
extern crate sel4_sys;

//#[macro_use]
extern crate log;

extern crate alloc;
use core::sync::atomic::{
    AtomicI8,
    Ordering::{
        Acquire,
        Relaxed,
        Release,
    },
};
use core::time::Duration;
use alloc::sync::Arc;
use sel4::Notification;

/*macro_rules! debug_println {
    ($($toks:tt)*) => ({
        #[cfg(feature = "debug")]
        info!($($toks)*);
    })
}*/
