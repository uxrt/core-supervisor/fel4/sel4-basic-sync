# sel4-basic-sync

This provides basic implementations of synchronization primitives on top of seL4
notifications. It is a Rust reimplementation of parts of libsel4sync. The main
use of this is for async queues in the UX/RT transport layer, which cannot use
usync like the process server uses internally, since they must be shareable
between VSpaces (usync only works within a single VSpace).

## Status

Completely unimplemented.
